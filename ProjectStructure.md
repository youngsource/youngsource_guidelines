# Project Structuring
##Context
Youngsource and Laudis projects tend to use a lot of smaller projects. In order to streamline the hosting and local development process, Youngsource developed a simple set of rules and guidelines.

The ultimate goal is to let the programmer be able to simply type: 
```bash 
git pull <service-url> && cd <service> && docker-compose up --build
```
to get a local development machine running.

This also allows the server manager be able to logically derive the service structure and design a clean and simple hosting implementation.

## Terminology

Any Youngsource or Laudis project **must** comply to the **rules** and **may** implement the following **guidelines**.

## Basics
The project root must always contain the following **files** (bold) and *folders* (italic).

- **.env.example** An example environment file that can be copied as an .env file to be immediately used in a local environment. It must also contain all possible keys that the project uses in the environment. The values in the file may be null.
- **Readme** A basic file explaining the purpose of the project optionally accompanied by some general guidelines, installation guide, etc.
- **.gitgnore** This file should at least contain all private files *(eg. .env)*, compiled *(eg. /build/\*)*, computed *(eg. /vendor/)* and editor configuration files *(eg. /.idea/)*
- **docker-compose** Self explanatory file defining the service interaction as per docker-compose.

A project root may also contain:
- **gitmodules** This file is maintained by git and defines the rules where all submodules are put.

A project root containing multiple services must also contain the following:

- *\<servicename>* A service should always have a dedicated directory in the project root with the service name equal to the directory name.

A project root to be hosted by our server must also contain:
- **Jenkinsfile** A script for our jenkins integration system.


## Service Implementation

Each project implementing a service that uses a feature described in the underlying folders, must use said folder name and location.

- **Dockerfile** A single docker file defining the  docker container that runs the entire service.

- *submodules* All submodules that are used as a library are placed here after a 
```git
git submodule init && git submodule sync && git submodule update --remote
```
- *logs* All log files of the program are located here.
- *src* All the source files of the program logic is located here.
- *public* The files directly exposed to the server software are located here.
- *resources* All non program logic associated files are located here.
- *conf* The configuration files of the program are  located here
- *testing* The testing logic is located here.

## Multiple service implementation

A project root containing multiple services is advised to not implement a service itself. It is best considered to act as a hosting implementation combining different (micro) services. This is done to improve portability of the micro services.

In the docker-compose file services and container names need to be named in the following pattern:
`project_service`. 

You can choose your own name for the service folders, however we provide following suggestions.

- api: general backend logic.
- proxy: webserver connecting the project to the outside network.
- neo4j: neo4j database
- redis: redis cache
- mariadb: mariadb database
- website: frontend


The build context is constricted to the service folder. 
``` yaml
   build:
       context: servicefoldername 
       dockerfile: Dockerfile

```
