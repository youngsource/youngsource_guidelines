# Guidelines

These guidelines serve as a means to generalise the handling of youngsource projects and doing so provide easier help in case of problems. 

In the [docker file](docker.md) you'll find the general structure to be used in your project.

If you happen to run into a problem you can check the [troubleshooting file](troubleshooting.md) to see if your problem was already handled.
