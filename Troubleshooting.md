# Troubleshooting  <!-- omit in toc -->
In this document you'll find troubleshooting issues on multiple topics.
- [Docker](#Docker)
## Docker
In this section we'll describe general steps to take when you encounter a docker related problem in your development environment. After each step you should check if your issue has been resolved.

The steps are executed in the root of your project unless otherwise specified.

1. `git pull`
2. ```git submodule sync && git submodule init && git submodule update --recursive  --remote```
3. ```docker-compose down && docker-compose up```
4. ```docker-compose down && docker-compose up --build```
5. ```docker-compose down && docker rm $(docker ps -a -q) && docker-compose up```
6. ```docker-compose down && docker rm -v $(docker ps -a -q) && docker-compose up```
7. ```docker-compose down && docker rm -v $(docker ps -a -q) && docker network prune && docker-compose up```
8. ```docker-compose down && docker rm -v $(docker ps -a -q) && docker network prune && docker volume prune && docker-compose up```
9. Ask help
